import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
    Die Klasse ReadIn dient zum Auslesen und speichern der Eingabedatei.
 */
public class ReadIn {

    private String file;
    private char[][] puzzle;

    ReadIn(String s) {
        file = s;
    }

    public char[][] read() {
        Path p = Paths.get(file);

        try {
            BufferedReader br = Files.newBufferedReader(p);

            int num = 0;
            String line;
            while ((line = br.readLine()) != null) {
                if (num == 0) {
                    puzzle = new char[Integer.valueOf(line)][Integer.valueOf(line)];

                    num++;
                    continue;
                }

                for (int column = 0; column < line.length(); column++) {
                    puzzle[num-1][column] = line.charAt(column);
                }
                num++;
            }
        } catch (IOException e) {
            System.err.println("Ein Fehler ist aufgetreten! Möglicherweise ist die Datei nicht vorhanden bzw. verfügbar...");
            e.printStackTrace();
        }
        return puzzle;
    }
}
