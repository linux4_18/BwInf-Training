public class Node {
    Node left, right;
    Puzzle value;

    public Node(Puzzle p) {
        value = p;
        left = null;
        right = null;
    }
}