public class Main {

    public static void main(String[] args) {
        ReadIn r = new ReadIn(args[0]);
        Puzzle puzzle = new Puzzle(r.read());

        BinaryTree tree = new BinaryTree(puzzle);

        for (int i = 0; i < 5; i++) {
            tree.insert(puzzle.rotateLeft());
            tree.insert(puzzle.rotateRight());

            //System.err.println(i);
        }

        tree.breadthFirstSearch();
    }
}
