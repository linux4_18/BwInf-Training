import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {
    private Node root;

    BinaryTree(Puzzle v) {
        root = new Node(v);
    }

    public void insert(Puzzle v) {
        root = insert_recursive(v, root);
    }

    private Node insert_recursive(Puzzle v, Node n) {
        if (n == null) {
            return new Node(v);
        }

        if (v.get_status() <  n.value.get_status()) {
            n.left = insert_recursive(v, n.left);
        } else {
            n.right = insert_recursive(v, n.right);
        }
        return n;
    }

    public void breadthFirstSearch() {
        if (root == null) {
            return;
        }

        Queue<Node> nodes = new LinkedList<>();
        nodes.add(root);

        while (!nodes.isEmpty()) {
            Node node = nodes.remove();

            node.value.print();
            System.out.println();

            if (node.left != null) {
                nodes.add(node.left);
            }

            if (node.right != null) {
                nodes.add(node.right);
            }
        }
    }
}