public class Puzzle {

    private int status;     // Standardwert:0, nach Links: -1, nach Rechts:+1
    private char[][] p;

    Puzzle(char[][] a) {
        p = a;
        status = 0;
    }

    public Puzzle rotateLeft() {
        reset_status();

        char[][] p_tmp = new char[p.length][p.length];

        for (int row = 0; row < p.length; row++) {
            for (int column = 0; column < p.length; column++) {
                p_tmp[column][(p.length - 1) - row] = p[row][column];
            }
        }
        p = p_tmp;
        status = -1;

        return this;
    }

    public Puzzle rotateRight() {
        reset_status();

        char[][] p_tmp = new char[p.length][p.length];

        for (int row = 0; row < p.length; row++) {
            for (int column = 0; column < p.length; column++) {
                p_tmp[(p.length - 1) - column][row] = p[row][column];
            }
        }
        p = p_tmp;
        status = 1;

        return this;
    }

    public void addGravity() {
        addGravity_horizontal();
        addGravity_vertical();
    }

    private void addGravity_horizontal() {
        for (int row = 1; row < p.length - 1; row++) {
            char comparisionElement = p[row][1];
            int groupLength = 0;
            int spaceCount = 0;

            for (int col = 1; col < p.length; col++) {
                if (comparisionElement == p[row][col]) {
                    if (p[row+1][col] == 32) {
                        spaceCount++;
                    }
                 groupLength++;
                } else {
                    if (spaceCount == groupLength) {
                        for (int i = (col-groupLength); i < groupLength+(col-groupLength); i++) {
                            p[row+1][i] = p[row][i];
                            p[row][i] = 32;
                        }
                    }
                    spaceCount = 0;
                    groupLength = 0;
                    comparisionElement = p[row][col];
                    col--;
                }
            }
        }
    }

    private void addGravity_vertical() {
        for (int col = 1; col < p.length; col++) {
            char comparisionElement = p[1][col];
            int groupLength = 0;
            int spaceCount = 0;

            for (int row = 1; row < p.length; row++) {
                if (comparisionElement == p[row][col]) {
                    groupLength++;
                } else if (p[row][col] == 32) {
                    spaceCount++;
                } else {
                    if (spaceCount > 0) {
                        int index = row-(spaceCount+groupLength);
                        char memory = p[row-(spaceCount+1)][col];

                        for (int i = 0; i < (groupLength+spaceCount); i++) {
                            if (i < groupLength) {
                                p[index][col] = 32;
                            } else {
                                p[index][col] = memory;
                            }
                            index++;
                        }
                    }
                    groupLength = 0;
                    spaceCount = 0;
                    comparisionElement = p[row][col];
                    row--;
                }
            }
        }
    }

    public void checkExit() {

    }

    //DEBUG: Output des Puzzles
    public void print() {
        for (int r = 0; r < p.length; r++) {
            for (int c = 0; c < p.length; c++) {
                System.out.print(p[r][c]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public Puzzle get() {
        return this;
    }

    public int get_status() {
        return status;
    }

    private void reset_status() {
        status = 0;
    }
}