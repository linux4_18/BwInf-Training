﻿namespace BwInf_T_Aufg1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.opt_OwWo = new System.Windows.Forms.RadioButton();
            this.opt_WwOo = new System.Windows.Forms.RadioButton();
            this.lblJahr = new System.Windows.Forms.Label();
            this.btnBerechnen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wann fällt....";
            // 
            // opt_OwWo
            // 
            this.opt_OwWo.AutoSize = true;
            this.opt_OwWo.Checked = true;
            this.opt_OwWo.Location = new System.Drawing.Point(15, 39);
            this.opt_OwWo.Name = "opt_OwWo";
            this.opt_OwWo.Size = new System.Drawing.Size(482, 21);
            this.opt_OwWo.TabIndex = 1;
            this.opt_OwWo.TabStop = true;
            this.opt_OwWo.Text = "...das orthodoxe Weihnachtsfest und das westliche Osterfest zusammen";
            this.opt_OwWo.UseVisualStyleBackColor = true;
            // 
            // opt_WwOo
            // 
            this.opt_WwOo.AutoSize = true;
            this.opt_WwOo.Location = new System.Drawing.Point(15, 66);
            this.opt_WwOo.Name = "opt_WwOo";
            this.opt_WwOo.Size = new System.Drawing.Size(486, 21);
            this.opt_WwOo.TabIndex = 2;
            this.opt_WwOo.Text = "...das westliche Weihnachtsfest und das orthodoxe Osterfest zusammen ";
            this.opt_WwOo.UseVisualStyleBackColor = true;
            // 
            // lblJahr
            // 
            this.lblJahr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJahr.Location = new System.Drawing.Point(108, 115);
            this.lblJahr.Name = "lblJahr";
            this.lblJahr.Size = new System.Drawing.Size(381, 25);
            this.lblJahr.TabIndex = 3;
            this.lblJahr.Text = "label2";
            this.lblJahr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBerechnen
            // 
            this.btnBerechnen.AutoSize = true;
            this.btnBerechnen.Location = new System.Drawing.Point(15, 115);
            this.btnBerechnen.Name = "btnBerechnen";
            this.btnBerechnen.Size = new System.Drawing.Size(87, 27);
            this.btnBerechnen.TabIndex = 4;
            this.btnBerechnen.Text = "Berechnen";
            this.btnBerechnen.UseVisualStyleBackColor = true;
            this.btnBerechnen.Click += new System.EventHandler(this.btnBerechnen_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 165);
            this.Controls.Add(this.btnBerechnen);
            this.Controls.Add(this.lblJahr);
            this.Controls.Add(this.opt_WwOo);
            this.Controls.Add(this.opt_OwWo);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "BwInf-T_Aufg1 | Sprichwort";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton opt_OwWo;
        private System.Windows.Forms.RadioButton opt_WwOo;
        private System.Windows.Forms.Label lblJahr;
        private System.Windows.Forms.Button btnBerechnen;
    }
}

