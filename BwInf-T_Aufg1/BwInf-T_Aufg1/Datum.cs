﻿namespace BwInf_T_Aufg1
{
    internal class Datum
    {
        int[] monate = { 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };   // Monate bzw. -tage

        int jahr;
        int monat;
        int tag;

        public Datum(int j, int m, int t)
        {
            jahr = j;
            monat = m;
            tag = t;
        }

        public Datum AddDays(int d)
        { 
            for (int i = d; i > 0; i--)
            {
                // Schaltjahr überprüfen
                if (IstSchaltjahr(jahr))
                {
                    monate[1] = 29;
                }
                else
                {
                    monate[1] = 28;
                }

                if (tag >= monate[monat-1])
                {
                    if (monat == 12)
                    {
                        jahr++;
                        monat = 1;
                    }
                    else
                    {
                        monat++;
                    }

                    tag = 1;
                }
                else
                {
                    tag++;
                }
            }
            return this;
        }

        public Datum AddYears(int y)
        {
            jahr += y;

            return this;
        }

        public static int CalcUnterschied(Datum d1, Datum d2)
        {
            Datum dummy = d1.Clone();
            int count;

            for (count = 0; !(dummy.tag == d2.tag && dummy.monat == d2.monat && dummy.jahr == d2.jahr); count++)
            {
                dummy.AddDays(1);
            }

            return count;
        }


        private bool IstSchaltjahr(int j)
        {
            if (j % 4 == 0) return true;
            if (j % 100 == 0) return false;
            if (j % 400 == 0) return true;

            return false;
        }

        public override string ToString() => tag + "." + monat + "." + jahr;

        public Datum Clone() => (Datum)this.MemberwiseClone();
    }
}