﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BwInf_T_Aufg1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBerechnen_Click(object sender, EventArgs e)
        {
            // Button deaktivieren
            btnBerechnen.Enabled = false;

            int jahr = 2016;
            Datum datWeihnachten = new Datum(jahr,12,25);

            int differenz = 0;

            if (opt_OwWo.Checked == true)
            {
                /* - Erste Teilaufgabe - */

                Datum datWeihnachten_jul;

                do
                {
                    datWeihnachten_jul = datWeihnachten.Clone();
                    datWeihnachten_jul.AddDays(Calc_difJulGreg(jahr));

                    differenz = Datum.CalcUnterschied(Calc_datOstern(jahr), datWeihnachten_jul);
                    
                    Console.WriteLine("INFO: | " + "Jahr: " + jahr + ", Ostern: " + Calc_datOstern(jahr) + ", Differenz (jul <-> greg): " + Calc_difJulGreg(jahr) + ", Differenz (O <-> W): " + differenz + ", Weihnachten: " + datWeihnachten_jul);    // Informationsausgabe

                    // Schleifen-Inkrementation
                    datWeihnachten.AddYears(1);
                    jahr++;
                }
                while (differenz != 365);   // Optimierungsbedarf :)

                lblJahr.Text = "Im Jahr: " + jahr;
                Console.WriteLine("FERTIG! | " + "Jahr: " + jahr + ", Ostern: " + Calc_datOstern(jahr) + ", Differenz (jul <-> greg): " + Calc_difJulGreg(jahr) + ", Differenz (O <-> W): " + differenz + ", Weihnachten: " + datWeihnachten_jul);      // Informationsausgabe
            }
            else
            {
                /* - Zweite Teilaufgabe - */

                /*do
                {
                    differenz = Datum.CalcUnterschied(Calc_datOstern_jul(jahr).AddDays(Calc_difJulGreg(jahr)), datWeihnachten);

                    Console.WriteLine("INFO: | " + "Jahr: " + jahr + ", Ostern: " + Calc_datOstern_jul(jahr).AddDays(Calc_difJulGreg(jahr)) + ", Differenz (jul <-> greg): " + Calc_difJulGreg(jahr) + ", Differenz (O <-> W): " + differenz + ", Weihnachten: " + datWeihnachten);    // Informationsausgabe

                    // Schleifen-Inkrementation
                    datWeihnachten.AddYears(1);
                    jahr++;
                }
                while (differenz != 365);*/

                lblJahr.Text = "Im Jahr: " + jahr;
                Console.WriteLine("FERTIG! | " + "Jahr: " + jahr + ", Ostern: " + Calc_datOstern_jul(jahr).AddDays(Calc_difJulGreg(jahr)) + ", Differenz (jul <-> greg): " + Calc_difJulGreg(jahr) + ", Differenz (O <-> W): " + differenz + ", Weihnachten: " + datWeihnachten);      // Informationsausgabe
            }
        }

        private int Calc_difJulGreg(int jahr)
        {
            int h = jahr / 100;
            int a = h / 4;
            int b = h % 4;

            return 3 * a + b - 2;
        }

        private Datum Calc_datOstern(int x)
        {
            /* Angaben der erweiterte gaußschen Osterfromel (wikipedia.org) */
            float k = x / 100;
            float m = 15 + (3 * k + 3) / 4 - (8 * k + 13) / 25;
            float s = 2 - (3 * k + 3) / 4;
            float a = x % 19;
            float d = (19 * a + m) % 30;
            float r = (d + a / 11) / 29;
            float og = 21 + d - r;
            float sz = 7 - (x + x / 4 + s) % 7;
            float oe = 7 - (og - sz) % 7;
            float os = og + oe;

            if (os > 31)
            {
                os -= 31;
                return new Datum(x, 4, (int)os);
            }
            return new Datum(x, 3, (int)os);
        }

        private Datum Calc_datOstern_jul(int x)
        {
            /* Angaben der erweiterte gaußschen Osterfromel (wikipedia.org) */
            float k = x / 100;
            float m = 15;
            float s = 0; 
            float a = x % 19;
            float d = (19 * a + m) % 30;
            float r = (d + a / 11) / 29;
            float og = 21 + d - r;
            float sz = 7 - (x + x / 4 + s) % 7;
            float oe = 7 - (og - sz) % 7;
            float os = og + oe;

            float os_ost = os + (x / 100) - (x / 400) - 2;

            if (os_ost > 31)
            {
                os_ost -= 31;
                return new Datum(x, 4, (int)os_ost);
            }
            return new Datum(x, 3, (int)os_ost);
        }
    }
}