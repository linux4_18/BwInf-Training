﻿using System;

public class Datum
{
    // Monate
    int[] monate = {31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    long jahr;
    int monat;
    int tag;

    public Datum()
    {
        jahr = 0;
        monat = 0;
        tag = 0;
    }

	public Datum(int j, int m, int t)
	{
        if (m == 2 && istSchaltjahr(j))
        {
            monate[1] = 28;
        }
        else if (m == 2) {
            monate[1] = 29;
        }

        jahr = j;
        monat = m;
        tag = t;
	}

    public void addDays(int d)
    {
        if ((tag+d) <= monate[monat--])
        {
            int ueberlauf = monate[monat--] - d;

            tag += d - ueberlauf;

            if (monat == 12)
            {
                jahr++;
                monat = 1;
            }

            monat++;
            tag = ueberlauf;
        } else {
            tag += d;
        }
    }

    public void addYears(int y)
    {
        jahr++;
    }

    private bool istSchaltjahr(int j)
    {
        if (jahr % 4 == 0) return true;
        if (jahr % 100 == 0) return false;
        if (jahr % 400 == 0) return true;
    }
}
