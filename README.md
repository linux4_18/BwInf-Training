# BwInf-Training
Meine bearbeiteten Aufgaben der vergangenen Bundeswettbewerbe für Informatik (kurz BwInf). Nur zu Trainingszwecken.

## Inhalt
| Projekt(name) | Aufgabe | Wettbewerb bzw. -jahr | Anmerkung |
| :----: | ----- | :-----: | ----- |
| BwInf-T_Aufg1 | Aufgabe 1 | 35. Wettbewerb (2016) |  |
| BwInf-T_Aufg2 | Aufgabe 3 | 35. Wettbewerb (2016) | eingestellt |
